# Tutorials

This is where tutorial sources are kept in [DocBook 4.5](https://tdg.docbook.org/tdg/4.5/docbook.html) format
(with SVG illustrations).

## Generating output

If you want to build a tutorial then you can use the scripts `make html`,
`make svg`, and `make all`.

- `make all [MYLANG=<LANG>]`  : Create HTML and SVG files (can be restricted to <LANG>)
- `make html [MYLANG=<LANG>]` : Create HTML files (can be restricted to <LANG>)
- `make svg [MYLANG=<LANG>]`  : Create SVG files (can be restricted to <LANG>). You will want to use the 'images' target before this.
- `make pot`                  : Update `tutorial-*.pot`
- `make po`                   : Update `tutorial-*.pot` and all `.po` files
- `make pretty-xml`           : Run xmlformat on source XML files
- `make check-input`          : Validate (non-compiled) input files
- `make check-output`         : Validate (compiled) output files
- `make copy-svg`             : Instructions for copying generated SVG tutorials to the correct directory
- `make copy-html`            : Instructions for copying generated HTML tutorials to the correct directory
- `make copy`                 : Instructions for copying both SVG and HTML tutorials
- `make clean`                : Clean all generated files
- `make clean-svg`            : Instructions for cleaning the SVG files in the tutorial directory
- `make clean-html`           : Instructions for cleaning the HTML files in the tutorial directory
- `make version`              : Print installed versions
- `make update-authors`       : Update `tutorial-*-authors.xml` with authors from git log

For help and additional usage information use `make help` and see `make-html`, `make-svg` files in this directory and the `README.md` file in the root directory of this repository.

## Requirements

- `python3`
- `xsltproc` and `xmllint`
- `tidy` for validating output HTML
- `inkscape` as command line tool
- `scour` (to compress output SVG files)
- `itstool` (to manage `.po` files)
- `xmlformat` (optional)

## Creating new tutorials

Place each tutorial and its illustrations into a subdirectory. An example is
basic/ with the Basic Tutorial.

Copy and use `template.xml` as skeleton to creating new tutorial source.
You find supported DocBook tags in this template. Please use only these tags and
preserve structure. You can validate XML width `xmllint --postvalid --noout your-tutorial.xml`

Create figures with provided template (`template-figure.svg`). Please
preserve canvas width (300px) and update canvas height to the content without margins
on the top and bottom. For consistent look please align drawing horizontally to the center
on the canvas. (It's important for HTML output to use `viewBox`). If you want to add (translatable)
labels follow the example.
